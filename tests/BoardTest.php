<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class Board 
{
	private $board = [ 
		[ 0, 0, 0 ],
		[ 0, 0, 0 ],
		[ 0, 0, 0 ],
    ];

	private function __construct()
	{
	}

	public static function create()
	{
		return new Board();
	}

	public function getBoard()
	{
		return $this->board;
	}
}

final class BoardTest extends TestCase
{
	public function testInitialStateOfBoard(): void
    {
    	$board = Board::create();
        $this->assertEquals(
            [ 
            	[ 0, 0, 0 ],
            	[ 0, 0, 0 ],
            	[ 0, 0, 0 ],
            ],
            $board->getBoard()
        );
    }
}
