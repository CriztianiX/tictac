<?php
function createNewBoard() {
  $board = [
    [null, null, null],
    [null, null, null],
    [null, null, null]
  ];

  return $board;
}


function checkIsOver($board, $rules) {
  foreach($rules as $key => $value) {
    if($value["coord"] && count( array_unique( $value["values"] ) ) === 1 ) {
            return $value["coord"];
    }
  }

  return null;
}

$board = createNewBoard();

$board[0][0] = "l";
$board[0][1] = null;
$board[0][2] = "O";


$board[1][0] = "a";
$board[1][1] = "O";
$board[1][2] = "d";

$board[2][0] = "O";
$board[2][1] = "f";
$board[2][2] = "g";


$a = [
  [ "coord" => $board[0][0], "values" => $board[0] ],
  [ "coord" => $board[1][0], "values" => $board[1] ],
  [ "coord" => $board[2][0], "values" => $board[2] ],

  [ "coord" => $board[0][0], "values" => [ $board[0][0], $board[1][0], $board[2][0] ] ],
  [ "coord" => $board[0][1], "values" => [ $board[0][1], $board[1][2], $board[2][2] ] ],
  [ "coord" => $board[0][2], "values" => [ $board[0][2], $board[1][2], $board[2][2] ] ],

  [ "coord" => $board[0][0], "values" => [ $board[0][0], $board[1][1], $board[2][2] ] ],
  [ "coord" => $board[0][2], "values" => [ $board[0][2], $board[1][1], $board[2][0] ] ],

];

var_dump(checkIsOver($board, $a));
