<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Match;

class MatchController extends Controller {

    public function index() {
        return view('index');
    }

    /**
     * Returns a list of matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches() {
        return response()->json($this->getMatches());
    }

    /**
     * Returns the state of a single match
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function match($id) {
        $match = Match::find($id);
        return response()->json($match);
    }

    /**
     * Makes a move in a match 
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function move($id) {
        $match = $this->getMatch($id);
        $board =  $match->board;

        $currentPlayer = Input::get('currentPlayer');
        $position = Input::get('position');

        if ( $board[$position] === null ) {
            $board[$position] = $currentPlayer;
        
            $match->next = $currentPlayer === 1 ? 2 : 1;
            $match->board = $board;
            //
            $match->save();

            return response()->json($match);
        }
    }

    /**
     * Creates a new match and returns the new list of matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create() {
        $match = new Match();
        $match->name = uniqid();
        $match->next = 1;
        $match->winner = null;
        $match->board = [
            null, null, null,
            null, null, null,
            null, null, null,
        ];

        $match->save();

        return response()->json($this->getMatches());
    }

    /**
     * Deletes the match and returns the new list of matches
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        $match = $this->getMatch($id);
        $match->delete();

        return response()->json($this->getMatches());
    }

    /**
     * Retrieve a single match
     *
     * @param $id
     * @return mixed
     */
    private function getMatch($id) {
        return Match::find($id);
    }

    /**
     * Retrieve array of matches
     *
     * @return \Illuminate\Support\Collection
     */
    private function getMatches() {
        $matches = Match::all();

        return $matches;
    }



    private function _move()
    {

    }
}